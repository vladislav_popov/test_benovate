from .base import *

DEBUG = False
SECRET_KEY = env('DJANGO_SECRET_KEY')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env('POSTGREE_DATABASE_NAME'),
        'USER': env('POSTGREE_USER'),
        'PASSWORD': env('POSTGREE_PASSWORD'),
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
