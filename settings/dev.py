from .base import *

SECRET_KEY = '123'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'test_benovate',
        'USER': 'benovate_admin',
        'PASSWORD': 'hardpassword',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
