# Test project for Benovate

### Questions:
* What if sender's inn and receiver's inn are same? (My solution is deny transaction)
* What if amount of transaction can't be divided by number of reciever's without remainder (My solution is round floor amount for user and change transaction amount to amount_for_one_after_round * recievers_count)

## Requirements:
* Python 3.6
* Django 2.2.1
* psql 11.2

## Usage:

### Apply migrations
` python manage.py migrate`

### Run tests
`python manage.py test`

### Start server
`python manage.py runserver`

### Urls:
`create_transaction/` - Django form with css
`api/create_transaction/` - endpoint for creating a transaction

## Author:
* Vladislav Popov
* povladal@gmail.com
* tg: @vup_vup