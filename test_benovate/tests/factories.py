import factory

from test_benovate.models import BenovateUser


class BenovateUserFactory(factory.django.DjangoModelFactory):
    username = factory.Sequence(lambda n: 'user%d' % n)

    class Meta:
        model = BenovateUser
