from decimal import Decimal
import json

from rest_framework.reverse import reverse
from rest_framework.status import (is_success, HTTP_201_CREATED, )
from rest_framework.test import APITestCase

from test_benovate.tests.factories import BenovateUserFactory


class CreateTransactionTestCase(APITestCase):
    def check_response_content(self, response, expected_content):
        self.assertEquals(json.loads(response.content.decode()), expected_content)

    def get_payload(self, sender_pk, inn_to, amount, path='create_transaction', format='json'):
        return {
            'path': reverse(path),
            'format': format,
            'data': {'sender': sender_pk, 'inn_to': inn_to, 'amount': amount}
        }

    def setUp(self):
        super().setUp()
        self.sender = BenovateUserFactory(wallet_balance=100.00,
                                          inn='11111')
        self.receiver1 = BenovateUserFactory(inn='22222')
        self.receiver2 = BenovateUserFactory(inn='22222')
        self.receiver3 = BenovateUserFactory(inn='22222')

    def test_create_ok(self):
        payload = self.get_payload(self.sender.pk, '22222', '10.00')
        response = self.client.post(**payload)
        self.assertEquals(response.status_code, HTTP_201_CREATED)

        self.sender.refresh_from_db()
        self.assertEqual(self.sender.wallet_balance, Decimal('90.01'))

        self.receiver1.refresh_from_db()
        self.receiver2.refresh_from_db()
        self.receiver3.refresh_from_db()
        self.assertEqual(self.receiver1.wallet_balance, Decimal('3.33'))
        self.assertEqual(self.receiver2.wallet_balance, Decimal('3.33'))
        self.assertEqual(self.receiver3.wallet_balance, Decimal('3.33'))

    def test_not_enough_balance(self):
        payload = self.get_payload(self.sender.pk, '22222', '101.00')
        response = self.client.post(**payload)
        self.assertFalse(is_success(response.status_code))
        expected_content = {"non_field_errors": ["This user doesn't have enough balance to make a transaction"]}
        self.check_response_content(response, expected_content)

    def test_receiver_with_same_inn(self):
        payload = self.get_payload(self.sender.pk, '11111', '50.00')
        response = self.client.post(**payload)
        self.assertFalse(is_success(response.status_code))
        expected_content = {"non_field_errors": ["You can't send money for sender's account"]}
        self.check_response_content(response, expected_content)

    def test_user_not_exists(self):
        payload = self.get_payload(1000, '11111', '50.00')
        response = self.client.post(**payload)
        self.assertFalse(is_success(response.status_code))
        expected_content = {'sender': ['Invalid pk "1000" - object does not exist.']}
        self.check_response_content(response, expected_content)

    def test_inn_not_exisis(self):
        payload = self.get_payload(self.sender.pk, '33333', '50.00')
        response = self.client.post(**payload)
        self.assertFalse(is_success(response.status_code))
        expected_content = {'inn_to': ['User with this Inn does not exists']}
        self.check_response_content(response, expected_content)

    def test_negative_amount_transaction(self):
        payload = self.get_payload(self.sender.pk, '22222', '-50.00')
        response = self.client.post(**payload)
        self.assertFalse(is_success(response.status_code))
        expected_content = {'amount': ['Transaction amount must be positive number']}
        self.check_response_content(response, expected_content)
