import json
import requests

from django.shortcuts import render
from django.urls import reverse

from test_benovate.forms import CreateTransactionForm


def create_transaction(request):
    if request.method == 'POST':
        form = CreateTransactionForm(request.POST)
        if form.is_valid():
            payload = {
                'sender': form.cleaned_data.get('sender').pk,
                'inn_to': form.cleaned_data.get('inn_to'),
                'amount': form.cleaned_data.get('amount')
            }
            print(form.cleaned_data.get('sender').pk)
            resp = requests.post(f'http://localhost:8000{reverse("create_transaction")}', data=payload)
            print(resp.content.decode())
            print(resp.status_code)
            if resp.ok:
                return render(request, 'create_transaction.html', {'form': CreateTransactionForm,
                                                                   'success': True})
            else:
                return render(request, 'create_transaction.html', {'form': CreateTransactionForm,
                                                                   'success': False,
                                                                   'errors': json.loads(resp.content.decode())})
    else:
        form = CreateTransactionForm

    return render(request, 'create_transaction.html', {'form': form})