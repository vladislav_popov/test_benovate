from django import forms

from test_benovate.models import BenovateUser


class CreateTransactionForm(forms.Form):
    sender = forms.ModelChoiceField(queryset=BenovateUser.objects.all())
    inn_to = forms.CharField(max_length=20)
    amount = forms.DecimalField(max_digits=18, decimal_places=2,)
