from django.contrib.auth.models import AbstractUser
from django.db import models

class BenovateUser(AbstractUser):
    wallet_balance = models.DecimalField(max_digits=18, decimal_places=2, default=0.00)
    inn = models.CharField(max_length=20, db_index=True, unique=False)

    def __str__(self):
        return f'User: {self.username}, balance: {self.wallet_balance}'
