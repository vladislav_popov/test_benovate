from rest_framework import serializers

from test_benovate.models import BenovateUser


class TransactionSerializer(serializers.Serializer):
    sender = serializers.PrimaryKeyRelatedField(queryset=BenovateUser.objects.all())
    inn_to = serializers.CharField(max_length=20)
    amount = serializers.DecimalField(max_digits=18, decimal_places=2)

    def validate_inn_to(self, inn):
        if not BenovateUser.objects.filter(inn=inn).exists():
            raise serializers.ValidationError('User with this Inn does not exists')
        else:
            return inn

    def validate_amount(self, amount):
        if amount <= 0:
            raise serializers.ValidationError('Transaction amount must be positive number')
        else:
            return amount

    def validate(self, attrs):
        if attrs['sender'].wallet_balance < attrs['amount']:
            raise serializers.ValidationError('This user doesn\'t have enough balance to make a transaction')
        if attrs['sender'].inn == attrs['inn_to']:
            raise serializers.ValidationError('You can\'t send money for sender\'s account')
        return attrs
