from decimal import Decimal, ROUND_FLOOR

from django.db.models import F
from django.db import transaction
from rest_framework import generics

from test_benovate.models import BenovateUser
from .serializers import TransactionSerializer


class TransactionCreateView(generics.CreateAPIView):
    serializer_class = TransactionSerializer

    def perform_create(self, serializer: TransactionSerializer):
        sender = BenovateUser.objects.get(pk=serializer.data['sender'])
        users_with_inn = BenovateUser.objects.filter(inn=serializer.data['inn_to'])
        transaction_amount = Decimal(serializer.data['amount'])
        amount_for_one = Decimal(transaction_amount) / users_with_inn.count()
        amount_for_one = amount_for_one.quantize(Decimal('1.00'), ROUND_FLOOR)
        with transaction.atomic():
            # if divide prime number by any amount there always will be a remainder which we don't want to be lost
            sender.wallet_balance = F('wallet_balance') - amount_for_one * users_with_inn.count()
            sender.save(update_fields=['wallet_balance', ])
            users_with_inn.update(wallet_balance=F('wallet_balance') + amount_for_one)
